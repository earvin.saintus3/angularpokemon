import { PokemonsService } from './../service/pokemons.service';
import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from '../model/pokemon';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pokemon-form',
  templateUrl: './pokemon-form.component.html',
  styleUrls: ['./pokemon-form.component.css']
})
export class PokemonFormComponent implements OnInit {

  @Input() pokemon: Pokemon;
  types: Array<string>;
  isAddForm: boolean;

  constructor(
    private pokemonsService: PokemonsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.types = this.pokemonsService.getPokemonType();
    this.isAddForm = this.router.url.includes('add');
    console.log(this.isAddForm);
  }
  // détermine si le type passé en paramètre appartient ou non au pokémon en cours
  hashType(type: string): boolean {
    let index = this.pokemon.types.indexOf(type);
    return (index > -1) ? true : false;
  }
  // appelée lorsque l'utilisateur ajoute ou retire un type au pokémon en cours
  selectType($event: any, type: string): void {
    let checked = $event.target.checked;
    if (checked) {
      this.pokemon.types.push(type);
    } else {
      let index = this.pokemon.types.indexOf(type);
      if (index > -1) {
        this.pokemon.types.splice(index, 1);
      }
    }
  }
  //valide le nombre de types pour chaque pokémon
  isTypesValid(type: string): boolean {
    if (this.pokemon.types.length === 1 && this.hashType(type)) {
      return false;
    }
    if (this.pokemon.types.length >= 3 && !this.hashType(type)) {
      return false;
    }
    return true;
  }

  // La méthode appelée lorsque le formulaire est soumis.
	onSubmit(): void {
		if (this.isAddForm) {
			this.pokemonsService.addPokemon(this.pokemon)
				.subscribe(pokemon => {
					this.pokemon = pokemon;
					this.goBack()
				});
		} else {
			this.pokemonsService.updatePokemon(this.pokemon)
				.subscribe(_ => this.goBack());
		}
	}

	goBack(): void {
		let link = ['/pokemon', this.pokemon.id];
		this.router.navigate(link);
	}

}
