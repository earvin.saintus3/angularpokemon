import { EditPokemonComponent } from './edit-pokemon/edit-pokemon.component';
import { DetailPokemonComponent } from './detail-pokemon/detail-pokemon.component';
import { ListPokemonComponent } from './list-pokemon/list-pokemon.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../auth.guard';

const pokemonsRoutes: Routes = [
  { path: 'pokemon', canActivate: [AuthGuard],
    children: [
      { path: 'all', component: ListPokemonComponent },
      { path: 'edit/:id', component: EditPokemonComponent },
      { path: ':id', component: DetailPokemonComponent }
    ]}
];

@NgModule({
  imports: [
    RouterModule.forRoot(pokemonsRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PokemonRoutingModule { }
