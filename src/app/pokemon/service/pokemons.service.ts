import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable, of } from 'rxjs';
import { Pokemon } from '../model/pokemon';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class PokemonsService {
  // le point d’accés à notre API
  pokemonsUrl = 'api/pokemons';

  constructor(private http: HttpClient) { }

  log(log: string) {
    console.log(log);
  }

  // gestion des erreurs
  // operation: nom de la methode qui a causé l'erreur
  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  // retourne tous les pokemons
  getPokemons(): Observable<Pokemon[]> {
		return this.http.get<Pokemon[]>(this.pokemonsUrl).pipe(
			tap(_ => this.log(`fetched pokemons`)),
			catchError(this.handleError('getPokemons', []))
		);
  }

  // retourne un pokemon avec un identifiant passé en paramètre
  getPokemon(id: number): Observable<Pokemon> {
		const url = `${this.pokemonsUrl}/${id}`;

		return this.http.get<Pokemon>(url).pipe(
			tap(_ => this.log(`fetched pokemon id=${id}`)),
			catchError(this.handleError<Pokemon>(`getPokemon id=${id}`))
		);
  }

  // modifie le pokemon dans le server
  updatePokemon(pokemon: Pokemon): Observable<any> {
		const httpOptions = {
			headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};

		return this.http.put(this.pokemonsUrl, pokemon, httpOptions).pipe(
			tap(_ => this.log(`updated pokemon id=${pokemon.id}`)),
			catchError(this.handleError<any>('updatePokemon'))
		);
  }

  addPokemon(pokemon: Pokemon): Observable<Pokemon> {
		const httpOptions = {
			headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};

		return this.http.post<Pokemon>(this.pokemonsUrl, pokemon, httpOptions).pipe(
			tap((pokemon: Pokemon) => this.log(`added pokemon with id=${pokemon.id}`)),
			catchError(this.handleError<Pokemon>('addPokemon'))
		);
  }

  deletePokemon(pokemon: Pokemon): Observable<Pokemon> {
		const url = `${this.pokemonsUrl}/${pokemon.id}`;
		const httpOptions = {
			headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};

		return this.http.delete<Pokemon>(url, httpOptions).pipe(
			tap(_ => this.log(`deleted pokemon id=${pokemon.id}`)),
			catchError(this.handleError<Pokemon>('deletePokemon'))
		);
  }

  searchPokemons(term: string): Observable<Pokemon[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Pokemon[]>(`${this.pokemonsUrl}/?name=${term}`).pipe(
      tap(_ => this.log(`found pokemons matching ${term}`)),
      catchError(this.handleError<Pokemon[]>(`searchPokemons`, []))
    );
  }

  getPokemonType(): string[] {
    return ['Plante', 'Feu', 'Eau', 'Insecte', 'Electrik', 'Poisson', 'Fée', 'Vol'];
  }

}
