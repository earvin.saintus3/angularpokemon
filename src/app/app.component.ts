import { Component, OnInit } from '@angular/core';
import { POKEMONS } from './pokemon/model/mock-pokemon';
import { Pokemon } from './pokemon/model/pokemon';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'pokemon';
  pokemons: Pokemon[] = null;

  ngOnInit(): void {
    this.pokemons = POKEMONS;
  }

  selectPokemon(pokemon: Pokemon){
    console.log('Vous avez selectionné' + pokemon.name);
  }

}
